package Snake.Snake;

import Snake.Map.Coordinates;

public interface SnakeListener {
	public void onAfterMove(Coordinates newPosition);

	public void onMoveUp(Coordinates newPostion, Coordinates oldPosition, int step);

	public void onMoveDown(Coordinates newPostion, Coordinates oldPosition, int step);

	public void onMoveLeft(Coordinates newPostion, Coordinates oldPosition, int step);

	public void onMoveRight(Coordinates newPostion, Coordinates oldPosition, int step);

	public void onRanIntoItself(Coordinates impactPosition);

	public void onRanIntoBorder(Coordinates beforeBorderPosition, int step);

	public void onOutOfBorder(Coordinates outOfBorderPosition);

	public void onEat(int newSnakeLength);
}