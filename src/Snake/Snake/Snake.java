package Snake.Snake;

import Snake.Map.*;
import processing.core.PApplet;
import processing.core.PConstants;

public class Snake {

	private final int SPAWN_POS_X = 1;
	private final int SPAWN_POS_Y = 1;
	/**
	 * e.g.: delayBetweenAutoMovement = 400;
	 * <p>
	 * 
	 * <pre>
	 * try {
	 * 	Thread.sleep(400);
	 * } catch (Exception e) {
	 * 	System.out.println("Automove Thread Exception: " + e.getMessage());
	 * }
	 * </pre>
	 */
	private final int delayBetweenAutoMovement = 270;
	private final int step = 1;

	private PApplet sketch;

	private float partLength;
	private float partWidth;
	private float[] headColor = { 0, 0, 150 };
	private float[] tailColor = { 0, 150, 0 };

	private int snakeLength;

	private Coordinates pos;
	private int autoMoveDirection;

	private boolean isMovementAllowed;

	private SnakeListener snakeListener;

	public void setSnakeListener(SnakeListener snakeListener) {
		this.snakeListener = snakeListener;
	}

	public Snake(PApplet sketch, GridMap parentMap) {
		pos = new Coordinates(SPAWN_POS_X, SPAWN_POS_Y, parentMap);
		pos.constrainMovement(true, true, true, true);

		autoMoveDirection = 0;
		isMovementAllowed = true;

		this.sketch = sketch;

		snakeLength = 0;

		this.partWidth = parentMap.getUnitHeight();
		this.partLength = parentMap.getUnitWidth();

	}

	public void display() {
		sketch.noStroke();
		sketch.fill(headColor[0], headColor[1], headColor[2]);
		sketch.rectMode(PConstants.CORNER);
		sketch.rect(pos.exactX(), pos.exactY(), partLength, partWidth);

		for (int i = pos.getPosHistory().size() - 1; i != (pos.getPosHistory().size() - 1 - snakeLength); i--) {
			sketch.fill(tailColor[0], tailColor[1], tailColor[2]);
			sketch.rect(pos.getPosHistory().get(i).exactX(), pos.getPosHistory().get(i).exactY(), partLength,
					partWidth);
		}
	}

	public void move(char inputKey) {
		if (isMovementAllowed && (inputKey == PConstants.CODED)) {
			switch (sketch.keyCode) {

			// Move upwards by step
			case PConstants.UP:
				if (!checkIfRanIntoItself(step, PConstants.UP)) {
					if (pos.moveUp(step) && snakeListener != null) {
						snakeListener.onRanIntoBorder(pos, step);
					}
				}

				if (snakeListener != null) {
					snakeListener.onMoveUp(pos, pos.lastPos(), step);
					if (pos.outOfBorder()) {
						snakeListener.onOutOfBorder(pos);
					}
				}
				break;

			// Move downwards by step
			case PConstants.DOWN:
				if (!checkIfRanIntoItself(step, PConstants.DOWN)) {
					if (pos.moveDown(step) && snakeListener != null) {
						snakeListener.onRanIntoBorder(pos, step);
					}
				}

				if (snakeListener != null) {
					snakeListener.onMoveDown(pos, pos.lastPos(), step);
					if (pos.outOfBorder()) {
						snakeListener.onOutOfBorder(pos);
					}
				}
				break;

			// Move to the left by step
			case PConstants.LEFT:
				if (!checkIfRanIntoItself(step, PConstants.LEFT)) {
					if (pos.moveLeft(step) && snakeListener != null) {
						snakeListener.onRanIntoBorder(pos, step);
					}
				}

				if (snakeListener != null) {
					snakeListener.onMoveLeft(pos, pos.lastPos(), step);
					if (pos.outOfBorder()) {
						snakeListener.onOutOfBorder(pos);
					}
				}
				break;

			// Move to the right by step
			case PConstants.RIGHT:
				if (!checkIfRanIntoItself(step, PConstants.RIGHT)) {
					if (pos.moveRight(step) && snakeListener != null) {
						snakeListener.onRanIntoBorder(pos, step);
					}
				}

				if (snakeListener != null) {
					snakeListener.onMoveRight(pos, pos.lastPos(), step);
					if (pos.outOfBorder()) {
						snakeListener.onOutOfBorder(pos);
					}
				}
				break;
			}// switch(sketch.keyCode)

			// snakeListener call
			if (snakeListener != null) {
				snakeListener.onAfterMove(pos);
			}
		}
	}

	public void setAutoMoveDirection(int inputKeyCode) {
		if (!checkIfOppositeDirection(inputKeyCode)) {
			autoMoveDirection = inputKeyCode;
		}
	}

	/**
	 * <tt>always returns false</tt> if direction is anything else than the
	 * following PConstants:
	 * <p>
	 * 
	 * <tt> direction = PConstants.UP;<br>
	 * direction = PConstants.DOWN;<br>
	 * direction = PConstants.RIGHT;<br>
	 * direction = PConstants.LEFT;
	 */
	private boolean checkIfRanIntoItself(int step, int direction) {
		if (direction == PConstants.UP) {
			for (int i = pos.getPosHistory().size() - 1; i != (pos.getPosHistory().size() - 1 - snakeLength); i--) {
				if (pos.hypotheticalMoveUp(step).equals((pos.getPosHistory().get(i)))) {
					if (snakeListener != null) {
						snakeListener.onRanIntoItself(pos);
					}
					return true;
				}
			}
			return false;
		} else if (direction == PConstants.DOWN) {
			for (int i = pos.getPosHistory().size() - 1; i != (pos.getPosHistory().size() - 1 - snakeLength); i--) {
				if (pos.hypotheticalMoveDown(step).equals((pos.getPosHistory().get(i)))) {
					if (snakeListener != null) {
						snakeListener.onRanIntoItself(pos);
					}
					return true;
				}
			}
			return false;
		} else if (direction == PConstants.RIGHT) {
			for (int i = pos.getPosHistory().size() - 1; i != (pos.getPosHistory().size() - 1 - snakeLength); i--) {
				if (pos.hypotheticalMoveRight(step).equals((pos.getPosHistory().get(i)))) {
					if (snakeListener != null) {
						snakeListener.onRanIntoItself(pos);
					}
					return true;
				}
			}
			return false;
		} else if (direction == PConstants.LEFT) {
			for (int i = pos.getPosHistory().size() - 1; i != (pos.getPosHistory().size() - 1 - snakeLength); i--) {
				if (pos.hypotheticalMoveLeft(step).equals((pos.getPosHistory().get(i)))) {
					if (snakeListener != null) {
						snakeListener.onRanIntoItself(pos);
					}
					return true;
				}
			}
			return false;
		} else {
			return false;
		}

	}

	public void eat() {
		snakeLength++;

		// snakeListener call
		if (snakeListener != null) {
			snakeListener.onEat(snakeLength);
		}
	}

	public void isMovementAllowed(boolean isAllowed) {
		isMovementAllowed = isAllowed;
	}

	public Coordinates getPos() {
		return pos;
	}

	public void respawn() {
		snakeLength = 0;
		autoMoveDirection = 0;
		pos.resetHistory();
		pos.setPosXY(SPAWN_POS_X, SPAWN_POS_Y);
	}

	/**
	 * <tt> 0 <= red <= 255 <br>
	 * 0 <= green <= 255 <br>
	 * 0 <= blue <= 255
	 */
	public void setHeadColor(float red, float green, float blue) {
		if (0 <= red && red <= 255) {
			headColor[0] = red;
		}
		if (0 <= green && green <= 255) {
			headColor[1] = green;
		}
		if (0 <= blue && blue <= 255) {
			headColor[2] = blue;
		}
	}

	/**
	 * <tt> 0 <= red <= 255 <br>
	 * 0 <= green <= 255 <br>
	 * 0 <= blue <= 255
	 */
	public void setTailColor(float red, float green, float blue) {
		if (0 <= red && red <= 255) {
			tailColor[0] = red;
		}
		if (0 <= green && green <= 255) {
			tailColor[1] = green;
		}
		if (0 <= blue && blue <= 255) {
			tailColor[2] = blue;
		}
	}

	public void startAutoMove() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (isMovementAllowed) {
					switch (autoMoveDirection) {

					// Move upwards by step
					case PConstants.UP:
						if (!checkIfRanIntoItself(step, PConstants.UP)) {
							if (pos.moveUp(step) && snakeListener != null) {
								snakeListener.onRanIntoBorder(pos, step);
							}
						}

						if (snakeListener != null) {
							snakeListener.onMoveUp(pos, pos.lastPos(), step);
							if (pos.outOfBorder()) {
								snakeListener.onOutOfBorder(pos);
							}
						}
						break;

					// Move downwards by step
					case PConstants.DOWN:
						if (!checkIfRanIntoItself(step, PConstants.DOWN)) {
							if (pos.moveDown(step) && snakeListener != null) {
								snakeListener.onRanIntoBorder(pos, step);
							}
						}

						if (snakeListener != null) {
							snakeListener.onMoveDown(pos, pos.lastPos(), step);
							if (pos.outOfBorder()) {
								snakeListener.onOutOfBorder(pos);
							}
						}
						break;

					// Move to the left by step
					case PConstants.LEFT:
						if (!checkIfRanIntoItself(step, PConstants.LEFT)) {
							if (pos.moveLeft(step) && snakeListener != null) {
								snakeListener.onRanIntoBorder(pos, step);
							}
						}

						if (snakeListener != null) {
							snakeListener.onMoveLeft(pos, pos.lastPos(), step);
							if (pos.outOfBorder()) {
								snakeListener.onOutOfBorder(pos);
							}
						}
						break;

					// Move to the right by step
					case PConstants.RIGHT:
						if (!checkIfRanIntoItself(step, PConstants.RIGHT)) {
							if (pos.moveRight(step) && snakeListener != null) {
								snakeListener.onRanIntoBorder(pos, step);
							}
						}

						if (snakeListener != null) {
							snakeListener.onMoveRight(pos, pos.lastPos(), step);
							if (pos.outOfBorder()) {
								snakeListener.onOutOfBorder(pos);
							}
						}
						break;
					}// switch(autoMoveDirection)

					if (snakeListener != null) {
						snakeListener.onAfterMove(pos);
					}

					try {
						Thread.sleep(delayBetweenAutoMovement);
					} catch (Exception e) {
						System.out.println("Automove Thread Exception: " + e.getMessage());
					}
				}
			}
		}).start();
	}

	private boolean checkIfOppositeDirection(int direction) {
		if (autoMoveDirection == PConstants.UP && direction == PConstants.DOWN) {
			return true;
		} else if (autoMoveDirection == PConstants.DOWN && direction == PConstants.UP) {
			return true;
		} else if (autoMoveDirection == PConstants.RIGHT && direction == PConstants.LEFT) {
			return true;
		} else if (autoMoveDirection == PConstants.LEFT && direction == PConstants.RIGHT) {
			return true;
		} else {
			return false;
		}
	}
}
