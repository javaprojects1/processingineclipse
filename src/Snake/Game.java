package Snake;

import Snake.Item.*;
import Snake.Map.*;
import Snake.Snake.*;
import processing.core.PApplet;

public class Game extends PApplet {
	public static void main(String[] args) {
		PApplet.main(Game.class.getName());
	}

	public void settings() {
		size(500, 500);
	}

	int columns = 12;
	int rows = 12;
	Snake mySnake;
	GridMap myMap;
	Item itemOnMap;

	public void setup() {
		background(155, 155, 155);

		myMap = new GridMap(this, columns, rows);
		mySnake = new Snake(this, myMap);
		itemOnMap = new Item(this, myMap);

		registerListeners();
		
		mySnake.startAutoMove();
	}

	public void draw() {
		background(155, 155, 155);
		mySnake.display();
		itemOnMap.checkIfEaten(mySnake.getPos());
		itemOnMap.display();
		myMap.display();
	}

	public void keyPressed() {
		if (key == 'w') {
			gameRestart();
		}
		mySnake.setAutoMoveDirection(keyCode);
	}

	private void gameOver() {
		mySnake.isMovementAllowed(false);
		mySnake.setHeadColor(255, 0, 255);
	}

	private void gameRestart() {
		itemOnMap.resetPosition();
		mySnake.setHeadColor(0, 0, 150);
		mySnake.isMovementAllowed(true);
		mySnake.respawn();
		mySnake.startAutoMove();
	}

	private void registerListeners() {
		mySnake.setSnakeListener(new SnakeListener() {

			@Override
			public void onAfterMove(Coordinates newPosition) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onEat(int newSnakeLength) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onOutOfBorder(Coordinates outOfBorderPosition) {
				System.out.println("\nonOutOfBorder: \nposX: " + outOfBorderPosition.X() + "\nposY: "
						+ outOfBorderPosition.Y() + "\n");

			}

			@Override
			public void onRanIntoBorder(Coordinates beforeBorderPosition, int step) {
				System.out.println("\nonRanIntoBorder: \nposX: " + beforeBorderPosition.X() + "\nposY: "
						+ beforeBorderPosition.Y() + "\nStep: " + step + "\n");
				gameOver();
			}

			@Override
			public void onMoveUp(Coordinates newPostion, Coordinates oldPosition, int step) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMoveDown(Coordinates newPostion, Coordinates oldPosition, int step) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMoveLeft(Coordinates newPostion, Coordinates oldPosition, int step) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMoveRight(Coordinates newPostion, Coordinates oldPosition, int step) {
			}

			@Override
			public void onRanIntoItself(Coordinates impactPosition) {
				// TODO Auto-generated method stub
				println("\nonRanIntoItself: \nposX: " + impactPosition.X() + "\nposY: " + impactPosition.Y() + "\n");
				gameOver();
			}
		});

		itemOnMap.setItemListener(new ItemListener() {

			@Override
			public void onEaten() {
				mySnake.eat();
			}

			@Override
			public void onAfterRespawn(Coordinates newPosition) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onBeforeRespawn(Coordinates newPosition) {
				// TODO Auto-generated method stub

			}

		});
	}

}