package Snake.Map;

import processing.core.PApplet;

public class GridMap {
	private PApplet sketch;

	private int amountOfColumns = 10;
	private int amountOfRows = 10;

	public GridMap(PApplet sketch, int amountOfColumns, int amountOfRows) {
		this.sketch = sketch;
		this.amountOfColumns = amountOfColumns;
		this.amountOfRows = amountOfRows;
	}

	public void display() {
		sketch.stroke(0, 0, 0, 255);
		//sketch.strokeWeight(2);

		float x = 0;
		for (int i = 1; i < amountOfColumns; ++i) {
			x = getUnitWidth() * i;
			sketch.line(x, 0, x, sketch.height);
		}

		float y = 0;
		for (int i = 1; i < amountOfRows; ++i) {
			y = getUnitHeight() * i;
			sketch.line(0, y, sketch.width, y);
		}
	}

	public int getColumns() {
		return amountOfColumns;
	}

	public int getRows() {
		return amountOfRows;
	}

	public float getUnitWidth() {
		return (float) sketch.width / amountOfColumns;
	}

	public float getUnitHeight() {
		return (float) sketch.height / amountOfRows;
	}

	public int getArea() {
		return amountOfColumns*amountOfRows;
	}
	
	public int getSketchWidth() {
		return sketch.width;
	}
	
	public int getSketchHeight() {
		return sketch.height;
	}
	
	public int getRandomPosX() {
		return (int) sketch.random(0, amountOfColumns);
	}
	
	public int getRandomPosY() {
		return (int) sketch.random(0, amountOfRows);
	}
	
}
