package Snake.Map;

import java.util.ArrayList;

public class Coordinates {
	private int posX;
	private int posY;
	private int constructorPosX;
	private int constructorPosY;

	private ArrayList<Coordinates> posHistory;

	private GridMap parentMap;

	private boolean isConstrainedMoveUp;
	private boolean isConstrainedMoveLeft;
	private boolean isConstrainedMoveRight;
	private boolean isConstrainedMoveDown;

//	CONSTRUCTORS---------------------------------------------------
	public Coordinates(GridMap parentMap) {
		posHistory = new ArrayList<Coordinates>();

		this.parentMap = parentMap;

		posX = 0;
		posY = 0;
		constructorPosX = 0;
		constructorPosY = 0;

		isConstrainedMoveUp = false;
		isConstrainedMoveLeft = false;
		isConstrainedMoveRight = false;
		isConstrainedMoveDown = false;
	}

	public Coordinates(int posX, int posY, GridMap parentMap) {
		posHistory = new ArrayList<Coordinates>();

		this.parentMap = parentMap;

		this.posX = posX;
		this.posY = posY;
		constructorPosX = posX;
		constructorPosY = posY;

		isConstrainedMoveUp = false;
		isConstrainedMoveLeft = false;
		isConstrainedMoveRight = false;
		isConstrainedMoveDown = false;
	}

//	PRIVATE METHODS	---------------------------------------------------
	private void savePosHistory() {
		if (posHistory.isEmpty()) {
			posHistory.add(new Coordinates(constructorPosX, constructorPosX, parentMap));
		} else {
			posHistory.add(new Coordinates(posX, posY, parentMap));
		}
	}

//	PUBLIC METHODS	---------------------------------------------------
	public boolean outOfBorder() {
		return (posY < 0 || posY >= parentMap.getRows() || posX < 0 || posX >= parentMap.getColumns()) ? true : false;
	}

	public boolean equalsX(int otherMapPosX) {
		return (posX == otherMapPosX) ? true : false;
	}

	public boolean equalsY(int otherMapPosY) {
		return (posY == otherMapPosY) ? true : false;
	}

	public boolean equalsXY(int otherMapPosX, int otherMapPosY) {
		return (posX == otherMapPosX && posY == otherMapPosY) ? true : false;
	}

	public boolean equals(Coordinates position) {
		return (position.X() == posX && position.Y() == posY) ? true : false;
	}

	// returns true if posX+step will be out of border else false.
	// Only works when isConstrainedMoveRight = true
	public boolean moveRight(int step) {
		if (isConstrainedMoveRight) {
			if (posX + step < parentMap.getColumns()) {

				savePosHistory();
				posX += step;

				return false;
			} else {
				return true;
			}
		} else {

			savePosHistory();
			posX += step;

			return false;
		}
	}

	public Coordinates hypotheticalMoveRight(int step) {
		return new Coordinates(posX + step, posY, parentMap);
	}

	// returns true if posX-step will be out of border else false.
	// Only works when isConstrainedMoveLeft = true
	public boolean moveLeft(int step) {
		if (isConstrainedMoveLeft) {
			if (posX - step >= 0) {

				savePosHistory();
				posX -= step;

				return false;
			} else {
				return true;
			}
		} else {

			savePosHistory();
			posX -= step;

			return false;
		}
	}

	// returns true if posY-step will be out of border else false.
	public Coordinates hypotheticalMoveLeft(int step) {
		return new Coordinates(posX - step, posY, parentMap);
	}

	// returns true if posY-step will be out of border else false.
	// Only works when isConstrainedMoveUp = true
	public boolean moveUp(int step) {
		if (isConstrainedMoveUp) {
			if (posY - step >= 0) {

				savePosHistory();
				posY -= step;

				return false;
			} else {
				return true;
			}
		} else {

			savePosHistory();
			posY -= step;

			return false;
		}
	}

	// returns true if posY+step will be out of border else false.
	public Coordinates hypotheticalMoveUp(int step) {
		return new Coordinates(posX, posY - step, parentMap);
	}

	// returns true if posY+step will be out of border else false.
	// Only works when isConstrainedMoveDown = true
	public boolean moveDown(int step) {
		if (isConstrainedMoveDown) {
			if (posY + step < parentMap.getRows()) {

				savePosHistory();
				posY += step;

				return false;
			} else {
				return true;
			}
		} else {

			savePosHistory();
			posY += step;

			return false;
		}
	}

	public Coordinates hypotheticalMoveDown(int step) {
		return new Coordinates(posX, posY + step, parentMap);
	}

	public void constrainMovement(boolean constrainMoveUp, boolean constrainMoveLeft, boolean constrainMoveRight,
			boolean constrainMoveDown) {
		isConstrainedMoveUp = constrainMoveUp;
		isConstrainedMoveLeft = constrainMoveLeft;
		isConstrainedMoveRight = constrainMoveRight;
		isConstrainedMoveDown = constrainMoveDown;
	}

	public void resetHistory() {
		posHistory.clear();
	}

	public Coordinates lastPos() {
		if (!posHistory.isEmpty()) {
			return posHistory.get(posHistory.size() - 1);
		} else {
			return new Coordinates(constructorPosX, constructorPosY, parentMap);
		}
	}

	// SETTERS ---------------------------------------------------
	public void setPosX(int newPosX) {
		savePosHistory();

		this.posX = newPosX;
	}

	public void setRandPosX() {
		savePosHistory();

		int oldX = posX;
		while (posX == oldX) {
			posX = parentMap.getRandomPosX();
		}

	}

	public void setPosY(int newPosY) {
		savePosHistory();

		this.posY = newPosY;
	}

	public void setRandPosY() {
		savePosHistory();

		int oldY = posY;
		while (posY == oldY) {
			posY = parentMap.getRandomPosY();
		}
	}

	public void setPosXY(int newPosX, int newPosY) {
		savePosHistory();

		this.posX = newPosX;
		this.posY = newPosY;
	}

	public void setRandPosXY() {
		savePosHistory();

		int oldX = posX;
		while (posX == oldX) {
			posX = parentMap.getRandomPosX();
		}

		int oldY = posY;
		while (posY == oldY) {
			posY = parentMap.getRandomPosY();
		}
	}

//	GETTERS			---------------------------------------------------
//	GETTERS			---------------------------------------------------
	public int X() {
		return posX;
	}

	public int Y() {
		return posY;
	}

	public float exactX() {
		return (posX / (float) parentMap.getColumns()) * parentMap.getSketchWidth();
	}

	public float exactY() {
		return (posY / (float) parentMap.getRows()) * parentMap.getSketchHeight();
	}

	public ArrayList<Coordinates> getPosHistory() {
		return posHistory;
	}

	public GridMap getParentMap() {
		return parentMap;
	}
}
