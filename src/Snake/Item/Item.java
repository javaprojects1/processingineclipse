package Snake.Item;

import Snake.Map.*;
import processing.core.PApplet;
import processing.core.PConstants;

public class Item {
	private final int SPAWN_POS_X = 3;
	private final int SPAWN_POS_Y = 3;
	
	private PApplet sketch;

	private float partLength = 0;
	private float partWidth = 0;

	private Coordinates pos;

	private ItemListener itemListener;

	public void setItemListener(ItemListener itemListener) {
		this.itemListener = itemListener;
	}

	public Item(PApplet sketch, GridMap parentMap) {
		pos = new Coordinates(SPAWN_POS_X, SPAWN_POS_Y, parentMap);

		this.sketch = sketch;

		this.partWidth = parentMap.getUnitHeight();
		this.partLength = parentMap.getUnitWidth();
	}

	public void display() {
		sketch.noStroke();
		sketch.fill(255, 0, 0, 255);
		sketch.ellipseMode(PConstants.CORNER);
		sketch.ellipse(pos.exactX(), pos.exactY(), partLength, partWidth);
	}

	public void respawn() {
		// itemListener call
		if (itemListener != null) {
			itemListener.onBeforeRespawn(pos);
		}

		pos.setRandPosXY();

		// itemListener call
		if (itemListener != null) {
			itemListener.onAfterRespawn(pos);
		}
	}

	public boolean checkIfEaten(Coordinates snakePosition) {
		if (snakePosition.equals(pos)) {

			if (itemListener != null) {
				itemListener.onEaten();
			}

			respawn();

			return true;
		} else {
			return false;
		}
	}

	public void resetPosition() {
		pos.setPosXY(SPAWN_POS_X, SPAWN_POS_Y);
	}
}
