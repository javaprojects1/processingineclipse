package Snake.Item;

import Snake.Map.Coordinates;

public interface ItemListener {
	public void onEaten();
	
	public void onAfterRespawn(Coordinates newPosition);
	
	public void onBeforeRespawn(Coordinates newPosition);
}
